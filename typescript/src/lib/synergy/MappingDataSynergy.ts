import { MondayItem } from "../../models/synergy/MondayModel";
import { SynergyModel } from "../../models/synergy/SynergyModel";


export const mappedToMondayItem = (item: MondayItem): SynergyModel => {
  const data: SynergyModel = {
    Description: item.name,
  };

  for (let i = 0; i < item.column_values.length; i++) {
    const value = item.column_values[i].text;
    switch (i) {
      case 0:
        data.CFUFU = value;
        break;
      case 1:
        data.Subtelkom = value;
        break;
      case 2:
        data.Startup = value;
        break;
      case 3:
        data.PIC = value;
        break;
      case 4:
        data.Grouping = value;
        break;
      case 5:
        data.Phase = value;
        break;
      case 6:
        data.Priority = value;
        break;
    }
  }

  return data;
}