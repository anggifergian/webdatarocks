import React, { useEffect, useState } from 'react';
import * as WebDataRocks from "react-webdatarocks";
import * as Highcharts from 'highcharts';

import { mappedToMondayItem } from './lib/synergy/MappingDataSynergy';
import { Monday } from './models/synergy/MondayModel';
import { wdrHeader } from './models/synergy/SynergyModel';

import "webdatarocks/webdatarocks.css";
import './App.css';

const App = () => {
  const [isLoading, setLoading] = useState<boolean>(false);
  const [telkomData, setTelkomData] = useState<any>({
    pipelineData: [],
    dropData: [],
    sustainData: [],
  });

  const ref: React.RefObject<WebDataRocks.Pivot> = React.useRef<WebDataRocks.Pivot>(null);

  const onLoading = () => {
    setTimeout(() => {
      setLoading(false);
    }, 800);
  }

  const fetchMonday = () => {
    fetch('https://api.monday.com/v2', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjEwNTYxODIzOSwidWlkIjoxNzY4MTI3MCwiaWFkIjoiMjAyMS0wNC0wN1QwMjozOTowMS4wMDhaIiwicGVyIjoibWU6d3JpdGUiLCJhY3RpZCI6NDk4MjcyOSwicmduIjoidXNlMSJ9.9MKwJOo9rZ4DyYyEy2efmIhwu9xMpM7HR2jX1ZTE2g8`,
      },
      body: JSON.stringify({
        query: `query { boards(ids: [730237972, 859406071]) {
        groups(ids: ["on_going_initiatives359", "new_group34830", "new_group15522", "topics", "duplicate_of_healthcare56913", "duplicate_of_healthcare", "duplicate_of_agriculture", "new_group"]) {
              title
              id
              items {
                id
                name
                column_values {
                  text
                }
              }
            }
        }}`,
      }),
    })
    .then(res => res.json())
    .then((res: Monday) => {
      setLoading(true);
      const mondayData = res.data.boards[1];
      const telkomData = mondayData.groups.map(group => (
        group.items.map(p => mappedToMondayItem(p))
      ));

      if (telkomData && telkomData.length > 0) {
        setTelkomData({
          pipelineData: [wdrHeader, ...telkomData[0]],
          dropData: [wdrHeader, ...telkomData[1]],
          sustainData: [wdrHeader, ...telkomData[2]],
        });
        onLoading();
      }
    });
  }

  useEffect(() => {
    fetchMonday();
  }, []);

  const onReportComplete = () => {
    if (ref.current) {
      ref.current.webdatarocks.off("reportcomplete");
      ref.current.webdatarocks.highcharts.getData(
        { type: "column" },
        (data: any) => {
          console.log('callback...', data);
          Highcharts.chart('highchartsContainer', data);
        },
        (data: any) => {
          console.log('update...', data);
          Highcharts.chart('highchartsContainer', data);
        }
      );
    }
  }

  return (
    <div className="App">
      {!isLoading && (
        <>
          <div id="highchartsContainer"></div>

          <div style={{padding: '12px'}}>
            {telkomData.pipelineData.length > 0 && (
              <WebDataRocks.Pivot
                ref={ref}
                toolbar={true}
                width="100%"
                reportcomplete={() => onReportComplete()}
                report={{
                  dataSource: {
                    dataSourceType: "json",
                    data: mondayJSONData()
                  },
                  slice: {
                    rows: [{
                      uniqueName: "CFUFU"
                    }, {
                      uniqueName: "Measures"
                    }],
                    columns: [{
                      uniqueName: "Phase"
                    }],
                    measures: [{
                      uniqueName: "Startup"
                    }]
                  },
                }}>
              </WebDataRocks.Pivot>
            )}
          </div>
        </>
      )}
    </div>
  );
}

function wdrJSONData() {
  return [{
    "Country": "Australia",
    "Category": "Wood",
    "Price": 445,
    "Revenue": 464
  }, {
    "Country": "Australia",
    "Category": "Bikes",
    "Price": 125,
    "Revenue": 440
  }, {
    "Country": "China",
    "Category": "Clothing",
    "Price": 190,
    "Revenue": 310
  }, {
    "Country": "United States",
    "Category": "Aircraft",
    "Price": 406,
    "Revenue": 127
  }, {
    "Country": "United States",
    "Category": "Bikes",
    "Price": 85,
    "Revenue": 821
  }, {
    "Country": "United Kingdom",
    "Category": "Cars",
    "Price": 21,
    "Revenue": 455
  }, {
    "Country": "Canada",
    "Category": "Clothing",
    "Price": 666,
    "Revenue": 685
  }, {
    "Country": "Germany",
    "Category": "Cars",
    "Price": 563,
    "Revenue": 742
  }, {
    "Country": "United Kingdom",
    "Category": "Bikes",
    "Price": 397,
    "Revenue": 340
  }, {
    "Country": "Germany",
    "Category": "Clothing",
    "Price": 800,
    "Revenue": 948
  }, {
    "Country": "Germany",
    "Category": "Cars",
    "Price": 172,
    "Revenue": 800
  }, {
    "Country": "Canada",
    "Category": "Aircraft",
    "Price": 352,
    "Revenue": 947
  }, {
    "Country": "United Kingdom",
    "Category": "Aircraft",
    "Price": 540,
    "Revenue": 328
  }, {
    "Country": "United Kingdom",
    "Category": "Aircraft",
    "Price": 204,
    "Revenue": 216
  }, {
    "Country": "Germany",
    "Category": "Clothing",
    "Price": 292,
    "Revenue": 644
  }, {
    "Country": "Canada",
    "Category": "Aircraft",
    "Price": 901,
    "Revenue": 237
  }, {
    "Country": "Canada",
    "Category": "Wood",
    "Price": 348
  }, {
    "Country": "Canada",
    "Category": "Clothing",
    "Price": 725,
    "Revenue": 335
  }, {
    "Country": "Canada",
    "Category": "Clothing",
    "Price": 13,
    "Revenue": 687
  }, {
    "Country": "China",
    "Category": "Wood",
    "Price": 62,
    "Revenue": 378
  }]
}

function mondayJSONData(): {
  CFUFU: 'Enterprise' | 'Mobile' | 'Digital Business',
  Subtelkom: 'Telkomsel' | 'Metranet' | 'Infomedia',
  Phase: 'Explore & Define' | 'Develop' | 'Drop' | 'Sustain',
  Startup: string,
}[] {
  return [
    {
      CFUFU: 'Digital Business',
      Subtelkom: 'Infomedia',
      Phase: 'Develop',
      Startup: 'X'
    },
    {
      CFUFU: 'Digital Business',
      Subtelkom: 'Infomedia',
      Phase: 'Develop',
      Startup: 'Y'
    },
    {
      CFUFU: 'Digital Business',
      Subtelkom: 'Infomedia',
      Phase: 'Sustain',
      Startup: 'Z'
    },
    {
      CFUFU: 'Digital Business',
      Subtelkom: 'Metranet',
      Phase: 'Explore & Define',
      Startup: 'X'
    },
    {
      CFUFU: 'Digital Business',
      Subtelkom: 'Metranet',
      Phase: 'Develop',
      Startup: 'Y'
    },
    {
      CFUFU: 'Digital Business',
      Subtelkom: 'Metranet',
      Phase: 'Sustain',
      Startup: 'Z'
    },
    {
      CFUFU: 'Digital Business',
      Subtelkom: 'Telkomsel',
      Phase: 'Develop',
      Startup: 'X'
    },
    {
      CFUFU: 'Digital Business',
      Subtelkom: 'Telkomsel',
      Phase: 'Sustain',
      Startup: 'Y'
    },
    {
      CFUFU: 'Digital Business',
      Subtelkom: 'Telkomsel',
      Phase: 'Explore & Define',
      Startup: 'Z'
    },

    {
      CFUFU: 'Enterprise',
      Subtelkom: 'Infomedia',
      Phase: 'Explore & Define',
      Startup: 'X'
    },
    {
      CFUFU: 'Enterprise',
      Subtelkom: 'Infomedia',
      Phase: 'Sustain',
      Startup: 'Y'
    },
    {
      CFUFU: 'Enterprise',
      Subtelkom: 'Infomedia',
      Phase: 'Explore & Define',
      Startup: 'Z'
    },
    {
      CFUFU: 'Enterprise',
      Subtelkom: 'Metranet',
      Phase: 'Sustain',
      Startup: 'X'
    },
    {
      CFUFU: 'Enterprise',
      Subtelkom: 'Metranet',
      Phase: 'Drop',
      Startup: 'Y'
    },
    {
      CFUFU: 'Enterprise',
      Subtelkom: 'Metranet',
      Phase: 'Sustain',
      Startup: 'Z'
    },
    {
      CFUFU: 'Enterprise',
      Subtelkom: 'Telkomsel',
      Phase: 'Drop',
      Startup: 'X'
    },
    {
      CFUFU: 'Enterprise',
      Subtelkom: 'Telkomsel',
      Phase: 'Drop',
      Startup: 'Y'
    },
    {
      CFUFU: 'Enterprise',
      Subtelkom: 'Telkomsel',
      Phase: 'Sustain',
      Startup: 'Z'
    },

    {
      CFUFU: 'Mobile',
      Subtelkom: 'Infomedia',
      Phase: 'Drop',
      Startup: 'X'
    },
    {
      CFUFU: 'Mobile',
      Subtelkom: 'Infomedia',
      Phase: 'Sustain',
      Startup: 'Y'
    },
    {
      CFUFU: 'Mobile',
      Subtelkom: 'Infomedia',
      Phase: 'Develop',
      Startup: 'Z'
    },
    {
      CFUFU: 'Mobile',
      Subtelkom: 'Metranet',
      Phase: 'Develop',
      Startup: 'X'
    },
    {
      CFUFU: 'Mobile',
      Subtelkom: 'Metranet',
      Phase: 'Explore & Define',
      Startup: 'Y'
    },
    {
      CFUFU: 'Mobile',
      Subtelkom: 'Metranet',
      Phase: 'Develop',
      Startup: 'Z'
    },
    {
      CFUFU: 'Mobile',
      Subtelkom: 'Telkomsel',
      Phase: 'Develop',
      Startup: 'X'
    },
    {
      CFUFU: 'Mobile',
      Subtelkom: 'Telkomsel',
      Phase: 'Explore & Define',
      Startup: 'Y'
    },
    {
      CFUFU: 'Mobile',
      Subtelkom: 'Telkomsel',
      Phase: 'Develop',
      Startup: 'Z'
    },
  ];
}

export default App;
