export interface MondayItem {
  id: string,
  name: string,
  column_values: {
    text: string;
  }[],
}

export interface Monday {
  account_id: number;
  data: {
    boards: {
      groups: {
        id: string,
        title: string,
        items: MondayItem[],
      }[],
    }[],
  };
}
