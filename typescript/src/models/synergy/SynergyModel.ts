export interface SynergyModel {
  Description?: string;
  CFUFU?: string;
  Subtelkom?: string;
  Startup?: string;
  PIC?: string;
  Grouping?: string;
  Phase?: string;
  Priority?: string;
}

export const wdrHeader = {
  CFUFU: {
    type: "string",
  },
  Description: {
    type: "string",
  },
  Grouping: {
    type: "string"
  },
  PIC: {
    type: "string"
  },
  Phase: {
    type: "string"
  },
  Priority: {
    type: "string"
  },
  Startup: {
    type: "string"
  },
  Subtelkom: {
    type: "string"
  }
};